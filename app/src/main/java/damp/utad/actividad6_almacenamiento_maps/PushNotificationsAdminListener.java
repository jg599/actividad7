package damp.utad.actividad6_almacenamiento_maps;

/**
 * Created by Yony on 23/01/16.
 */
public interface PushNotificationsAdminListener {

    public void pushNotificationsRegistered(boolean blRegistered);
}
