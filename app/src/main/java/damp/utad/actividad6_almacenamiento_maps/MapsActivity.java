package damp.utad.actividad6_almacenamiento_maps;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.widget.Button;
import android.widget.Toast;

import com.example.germy.quickblox.QB_ubicacion_listener;
import com.facebook.FacebookSdk;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.quickblox.customobjects.model.QBCustomObject;

import java.util.ArrayList;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, LocationListener , QB_ubicacion_listener {

    private GoogleMap mMap;

    fragment_youtube fragment_you = new fragment_youtube();

    LocationManager locManager;
    String location_provider = LocationManager.GPS_PROVIDER;
    Location loc;
    Marker MKmaps;

    button_listener btn_listener ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        btn_listener = new button_listener(this);
        final Button boton_loc = (Button) findViewById(R.id.button_loc);
        boton_loc.setOnClickListener(btn_listener);



                // Perform action on click


        locManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    public void requestPermissions(@NonNull String[] permissions, int requestCode)
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for Activity#requestPermissions for more details.
                return;
            }
        }
        locManager.requestLocationUpdates(location_provider, 0, 0, this);
        loc = locManager.getLastKnownLocation(location_provider);

        if (!locManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            Toast.makeText(MapsActivity.this, " El gps no está activado ! ", Toast.LENGTH_SHORT).show();
        }


    }

@Override
    public void onLocationChanged(Location location) {


        double latitud = location.getLatitude();
        double longitud = location.getLongitude();
        double precision = location.getAccuracy();
        LatLng miloc = new LatLng(latitud, longitud);

        mMap.addMarker(new MarkerOptions().position(miloc).title("Marker in miloc"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(miloc));
        Toast.makeText(getApplicationContext(), "latitud = " + latitud + " longitud " + longitud + " precision " + precision, Toast.LENGTH_SHORT).show();
        if (MKmaps != null) {
            MKmaps.remove();
        }
    }

    public void onProviderDisabled(String provider) {
        Toast.makeText(MapsActivity.this, " provedorr off ! ", Toast.LENGTH_SHORT).show();
    }

    public void onProviderEnabled(String provider) {
        Toast.makeText(MapsActivity.this, "proveedor on ! ", Toast.LENGTH_SHORT).show();
    }

    public void onStatusChanged(String provider, int status, Bundle extras) {
        Toast.makeText(MapsActivity.this, " Estado! ", Toast.LENGTH_SHORT).show();
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;



    }

    @Override
    public void get_loc(ArrayList<QBCustomObject> customObjects, Bundle params) {

        for (int i = 0; i < customObjects.size(); i++) {
            double longitud = Double.parseDouble(String.valueOf(customObjects.get(i).getFields().get("longitud")));
            double latitud = Double.parseDouble(String.valueOf(customObjects.get(i).getFields().get("latitud")));

         
            LatLng Ponfe = new LatLng(latitud, longitud);


            mMap.addMarker(new MarkerOptions().position(Ponfe).title("Marker in Ponferrada"));


            String enlace_you =String.valueOf(customObjects.get(i).getFields().get("you"));
            mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(Marker marker) {

                    getSupportFragmentManager().beginTransaction().replace(R.id.content,fragment_you).commit();
                    return false;
                }
            });

        }



    }



}
