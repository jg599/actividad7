package damp.utad.actividad6_almacenamiento_maps;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.germy.quickblox.QBAdmin;
import com.example.germy.quickblox.QBUsersListener;
import com.example.germy.quickblox.QBUsers_Login;
import com.example.germy.quickblox.QB_Listener;


public class Login extends AppCompatActivity implements QB_Listener, QBUsersListener {
//declaro las variables
    private TextView lblGotoRegister;
    private Button btnLogin;
    private EditText username;
    private EditText inputPassword;

    private String nombre;
    private String password;
    private CheckBox box_recordar;
    QBAdmin qbAdmin;
    QBUsers_Login qbuser;




    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

//relacion entre las variables y el layout
        username = (EditText) findViewById(R.id.nombre);
        inputPassword = (EditText) findViewById(R.id.txtPass);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        box_recordar = (CheckBox) findViewById(R.id.box_recordar);

        qbAdmin = new QBAdmin();

        qbuser = new QBUsers_Login();

        qbAdmin.setListener(this);

        qbuser.addQBUserLoginListener(this);


        qbAdmin.setListener(this);
        qbAdmin.crear_sesion();
        btnLogin.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                String nombre = username.getText().toString();
                String password = inputPassword.getText().toString();
                logear(nombre,password);

            }
        });


    }


    @Override
    public void crear_sesion(boolean check) {
        SharedPreferences preferencias = getSharedPreferences("Almacenamiento_maps", 0);
        String usuario = preferencias.getString("USUARIO", null);
        String pass = preferencias.getString("PASS", null);





        if (check == true) {

            if (usuario != null && pass != null) {
                logear(usuario, pass);
            }
            Log.v("sessionCreada", " session creada " + check);

        } else {
            Log.v("sessionCreada", " session NO creada " + check);
        }
    }


    public void logear(String nombre, String password) {

        nombre = String.valueOf(username.getText());
        password = String.valueOf(inputPassword.getText());

        qbuser.loginUsuario(nombre, password);
    }

    @Override
    public void login(boolean logeado) {
        SharedPreferences preferencias = getSharedPreferences("usuarios_app6", Context.MODE_PRIVATE);
        if (logeado == true) {
//cuando se marque el checbox se guardara el usuario y la contraseña
            if (box_recordar.isChecked()) {
                SharedPreferences.Editor editor = preferencias.edit();
                editor.putString("USUARIO", nombre);
                editor.putString("PASS", password);
                editor.commit();
            }

            abrir_mapa();

//etiqueta de aviso de login
            Toast.makeText(Login.this, username.getText() + " Ha logueado con éxito ", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(Login.this, " Usuario o contraseña inválidos ", Toast.LENGTH_SHORT).show();
        }

    }


//intent para abrir el mapa
    public void abrir_mapa() {
        Intent i = new Intent(this, MapsActivity.class);
        startActivity(i);
    }


}
