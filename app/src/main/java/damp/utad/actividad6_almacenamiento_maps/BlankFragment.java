package damp.utad.actividad6_almacenamiento_maps;


import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;


/**
 * A simple {@link Fragment} subclass.
 */
public class BlankFragment extends Fragment {

    Button btn_fb;
    Button btn_tw;
    ShareDialog shareDialog;
    CallbackManager callbackManager;
    public BlankFragment() {
        // Required empty public constructor
    }


    @Override

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_blank, container, false);


        btn_fb = (Button) v.findViewById(R.id.btn_facebook);
        btn_tw = (Button) v.findViewById(R.id.btn_twiter);

        return v;

    }

        public void initSocialObjects(){

           // shareDialog = new ShareDialog(this);

            callbackManager = CallbackManager.Factory.create();

            shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {

                @Override

                public void onSuccess(Sharer.Result result) {}



                @Override

                public void onCancel() {}



                @Override

                public void onError(FacebookException error) {}

            });
        if (ShareDialog.canShow(ShareLinkContent.class)) {

            ShareLinkContent linkContent = new ShareLinkContent.Builder()

                    .setContentTitle("Hello Facebook")

                    .setContentDescription("The 'Hello Facebook' sample  showcases simple Facebook integration")



                    .setContentUrl(Uri.parse("http://developers.facebook.com/android"))

                    .build();



            shareDialog.show(linkContent);

        }


    }
    }



