package com.example.germy.quickblox;

import android.os.Bundle;

import com.quickblox.auth.QBAuth;
import com.quickblox.auth.model.QBSession;
import com.quickblox.core.QBEntityCallbackImpl;
import com.quickblox.core.QBSettings;

import java.util.List;


public class QBAdmin {




    QB_Listener listener = null;

    public QB_Listener getListener() {
        return listener;
    }

    public void setListener(QB_Listener listener) {
        this.listener = listener;
    }


   public QBAdmin(){
        QBSettings.getInstance().fastConfigInit("33806", "WExx4MNu87U-O3m", "MHTjYuacY5gwuE9");
    }

    public void crear_sesion(){
        QBAuth.createSession(new QBEntityCallbackImpl<QBSession>() {
            @Override
            public void onSuccess(QBSession session, Bundle params) {
                // success


                if (listener != null) {
                    //crea la sesision
                    listener.crear_sesion(true);
                }
            }

            @Override
            public void onError(List<String> errors) {
                // errors
                //Intento de crear sesion fallida
                if (listener != null) {
                    listener.crear_sesion(false);
                }
            }
        });

    }

}
